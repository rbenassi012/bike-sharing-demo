import { TestBed } from '@angular/core/testing';
import { AuthGardService } from './auth-gard.service';

import { JwtModule } from '@auth0/angular-jwt';
import { RouterModule } from '@angular/router';
import { RoutingModule } from 'src/app/component/routing/routing.module';
import { HomeComponent } from 'src/app/component/home/home.component';
import { LoginComponent } from 'src/app/component/login/login.component';
import { RegisterComponent } from 'src/app/component/register/register.component';
import { ShowUserComponent } from 'src/app/component/show-user/show-user.component';
import { ShowTravelsComponent } from 'src/app/component/show-travels/show-travels.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {MessageModule} from 'primeng/message';
import {FieldsetModule} from 'primeng/fieldset';
import {ButtonModule} from 'primeng/button';
import { StationComponent } from 'src/app/component/station/station.component';
import {GMapModule} from 'primeng/gmap';
import {ChartModule} from 'primeng/chart';
import {CardModule} from 'primeng/card';
import { TableModule } from 'primeng/table';
import {DialogModule, Dialog} from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import {MessagesModule} from 'primeng/messages';
import { FormsModule } from '@angular/forms';
import { InputMaskModule } from 'primeng/primeng';
import { APP_BASE_HREF } from '@angular/common';

export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}
export function tokenGetter() {
  return localStorage.getItem('currentUser');
}


describe('AuthGardService', () => {
  let fixture;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpClient,
      HttpHandler,
      {provide: APP_BASE_HREF, useValue: '/'}

    ],
    imports: [
      RouterModule,
      JwtModule.forRoot({
        config: {
          tokenGetter: tokenGetter
        }
      }),
      RoutingModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
      MessageModule,
      FieldsetModule,
      ButtonModule,
      GMapModule,
      ChartModule,
      CardModule,
      TableModule,
      DialogModule,
      DropdownModule,
      MessagesModule,
      FormsModule,
      InputMaskModule

    ],
    declarations: [
      LoginComponent,
      HomeComponent,
      RegisterComponent,
      ShowUserComponent,
      ShowTravelsComponent,
      StationComponent,
    ],

  }));

  it('should be created', () => {
    const service: AuthGardService = TestBed.get(AuthGardService);
    expect(service).toBeTruthy();
  });
});
