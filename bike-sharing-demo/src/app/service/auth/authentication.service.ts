import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../model/models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
  }

  login(username: String, password: String) {


    const apiUrl = environment.DB_AUTH;

    return this.http.post<any>(apiUrl, { username, password })
      .pipe(map(user => {

        // login successful if there's a jwt token in the response
        console.log(user);

        if (user && user.access_token) {

          console.log('setto token');
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          console.log(user.access_token);
          localStorage.setItem('currentUser', user.access_token);

        }

        return user;
      }));
  }

  addUser(user: any) {
    console.log(user);
    return this.http.post<any>(environment.DB_USER, <JSON>user );
  }

  logout() {
    console.log('logout');
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }
}
