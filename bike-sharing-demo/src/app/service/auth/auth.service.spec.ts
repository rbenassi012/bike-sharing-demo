import { TestBed } from '@angular/core/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthService } from './auth.service';

export function tokenGetter() {
  return localStorage.getItem('currentUser');
}

describe('AuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({
     imports: [JwtModule.forRoot({
       config: {
         tokenGetter: tokenGetter
       }
     })]
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });
});
