import { TestBed, inject } from '@angular/core/testing';

import { ServiceBikeService } from './service-bike.service';
import { HttpClientModule } from '@angular/common/http';

describe('ServiceBikeService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: ServiceBikeService = TestBed.get(ServiceBikeService);
    expect(service).toBeTruthy();
  });
});
