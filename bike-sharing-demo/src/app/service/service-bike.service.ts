import { Injectable } from '@angular/core';
import { User } from '../model/models';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class ServiceBikeService {

  constructor(private http: HttpClient) { }

  // insert utente VECCHIO
  addUserHttp(utente: User): Observable<User> {
    console.log('servizio');
  /** POST: ADD a new hero to the json server
   * : Observable<Utente>
  */
    return this.http.post<User>(environment.DB_USER, utente, httpOptions);
  }

  // take stations
  takeStation() {
    return this.http.get<any>(environment.DB_STAT);
  }

  takeBike(idBike: String) {
    console.log('Stampo nel service id bike:' + idBike);
    return this.http.get<any>(`${environment.DB_BIKE}/${idBike}`);
  }

  takeUser(username: String) {
    console.log('stampo nel service username' + username);
    return this.http.get<any>(`${environment.DB_USER}/${username}`);
  }

  borrowBike(travel: any) {
    console.log(travel);
    return this.http.post<any>(environment.DB_TRAV, <JSON>travel);
  }

  takeTravels(username: String) {
    // console.log(username);
    return this.http.get<any>(`${environment.DB_TRAV}/${username}`);
  }
  takeTravelsId(idTravel: String) {
    // console.log(idTravel)
    return this.http.get<any>(`${environment.DB_TRAVID}/${idTravel}`);
  }

  insertTravel(travel: any) {
    return this.http.post<any>(environment.DB_TRAVID, <JSON>travel);
  }

  takeColors() {
    return this.http.get<any>(`${environment.DB_COL}`);
  }

  updateUser(user: User){
    console.log("inside service");
    console.log(user);
    return this.http.post<any>(`${environment.DB_USER}/${user.username}`, user);
  } 

}
