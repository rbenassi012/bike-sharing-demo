export class User {
  name: String;
  surname: String;
  username: String;
  email: String;
  password: String;
  phone: String;
  image: String;
  numTelaio: String;
}
export interface Bike {
  numTelaio: String;
  description: String;
  image: String;
  model: String;
  free: boolean;
}
export class Travel {
  idTravel: String;
  idBike: String;
  idUser: String;
  dateArrival: number;
  dateDeparture: number;
  idArrivalStation: String;
  idDepartureStation: String;
  period: number;
}
export interface Station {
  idStation: String;
  bikes: Array <Bike>;
  name: String;
  coordinates: Array<number>;
}
export interface Colors {
  color: String;
}


