import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NavBarComponent } from '../app/component/nav-bar/nav-bar.component';
import { TabMenuModule } from 'primeng/tabmenu';
import { LoginComponent } from '../app/component/login/login.component';
import {ButtonModule} from 'primeng/button';

import { Routes, RouterModule, CanActivate } from '@angular/router';
import { RegisterComponent } from '../app/component/register/register.component';
import { HttpClientModule } from '@angular/common/http';
import {CardModule} from 'primeng/card';

import { FormsModule } from '@angular/forms';
import {MenubarModule} from 'primeng/menubar';

import { ShowTravelsComponent } from '../app/component/show-travels/show-travels.component';
import { ShowUserComponent } from '../app/component/show-user/show-user.component';
import { HomeComponent } from '../app/component/home/home.component';
import { StationComponent } from '../app/component/station/station.component';

import {DataViewModule} from 'primeng/dataview';
import {PanelModule} from 'primeng/panel';
import {OrderListModule} from 'primeng/orderlist';

import { ShowBikeComponent } from '../app/component/show-bike/show-bike.component';
import {DialogModule, Dialog} from 'primeng/dialog';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';

import { CarouselModule } from 'primeng/carousel';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DropdownModule } from 'primeng/dropdown';
import { AuthGardService } from '../app/service/auth/auth-gard.service';
import { RoutingModule} from '../app/component/routing/routing.module';

import { InputTextModule} from 'primeng/inputtext';
import { PasswordModule} from 'primeng/password';

import {FieldsetModule} from 'primeng/fieldset';
import {JwtModule} from '@auth0/angular-jwt' ;

import {ChartModule} from 'primeng/chart';

import {InputMaskModule} from 'primeng/inputmask';

import {GMapModule} from 'primeng/gmap';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


export function tokenGetter() {
  return localStorage.getItem('currentUser');
}

// Aot requires an expoted function for factories
export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoginComponent,
    RegisterComponent,
    ShowTravelsComponent,
    ShowUserComponent,
    HomeComponent,
    StationComponent,
    ShowBikeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TabMenuModule,
    ButtonModule,
    FormsModule,
    HttpClientModule,
    CardModule,
    DataViewModule,
    PanelModule,
    OrderListModule,
    DialogModule,
    CarouselModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    TableModule,
    OverlayPanelModule,
    DropdownModule,
    RoutingModule,
    InputTextModule,
    PasswordModule,
    FieldsetModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    ChartModule,
    InputMaskModule,
    GMapModule,
    MenubarModule,
    TranslateModule.forRoot({
      loader : {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
