import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from "@angular/common";


//components
import { ShowTravelsComponent } from './show-travels.component';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { StationComponent } from '../station/station.component';

//import primeng
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { MessageModule } from 'primeng/message';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { CardModule } from 'primeng/card';
import { GMapModule } from 'primeng/gmap';
import {MessagesModule} from 'primeng/messages';
import {ChartModule} from 'primeng/chart';
import {InputMaskModule} from 'primeng/inputmask';


//services
import { RoutingModule} from '../routing/routing.module';
import { AuthGardService } from '../../service/auth/auth-gard.service';
import {JwtModule} from '@auth0/angular-jwt' ;

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, Routes } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
import { ShowUserComponent } from '../show-user/show-user.component';
import { APP_BASE_HREF } from '@angular/common';

export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}
export function tokenGetter() {
  return localStorage.getItem('currentUser');
}

import { routes } from '../routing/routing.module';
import { ServiceBikeService } from 'src/app/service/service-bike.service';

describe('ShowTravelsComponent', () => {
  let component: ShowTravelsComponent;
  let fixture: ComponentFixture<ShowTravelsComponent>;

  let location: Location;
  let router : Router;
  //let fixture;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler,
        //ServiceBikeService,
        {provide: APP_BASE_HREF, useValue: 'showTravels'}
      ],
      imports: [
        TableModule,
        RouterTestingModule.withRoutes(routes),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        JwtModule.forRoot({
          config: {
            tokenGetter: tokenGetter
          }
        }),
        DialogModule,
        DropdownModule,
        ButtonModule,
        FormsModule,
        MessageModule,
        HttpClientModule,
        RoutingModule,
        FieldsetModule,
        CardModule,
        GMapModule,
        MessagesModule,
        ChartModule,
        InputMaskModule
      ],
      declarations: [
        ShowTravelsComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        ShowUserComponent,
        StationComponent
       ]
    })
    //router = TestBed.get(Router);
    // location = TestBed.get(Location);

    fixture = TestBed.createComponent(ShowTravelsComponent);
    component = fixture.componentInstance;

    //router.initialNavigation();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
