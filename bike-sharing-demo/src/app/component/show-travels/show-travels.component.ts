import { Component, OnInit } from '@angular/core';
import { ServiceBikeService } from '../../service/service-bike.service';
import { User, Travel, Station } from '../../model/models';
import * as JWT from 'jwt-decode';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-show-travels',
  templateUrl: './show-travels.component.html',
  styleUrls: ['./show-travels.component.css']
})
export class ShowTravelsComponent implements OnInit {

  travels: Travel[];
  // contiene id Viaggio da concludere
  selectedTravel: String;
  displayDialog: boolean;
  selectedStation: Station;
  stations: Station[];
  controlSelectStation = true;
  travelConcluded: Travel;
  val: number;
  msgs = [];

  constructor(private serviceBike: ServiceBikeService, private router: Router, private translate: TranslateService) { }

  ngOnInit() {
    const username = this.takeUser();

    this.serviceBike.takeTravels(username).subscribe(res => {
      this.travels = <Travel[]>res.reverse();
    });
  }

  // selezione travel da consludere
  selectTravel(idTravel: String) {
    // chiamata per il menu  tendina
    this.takeStations();

    this.selectedTravel = idTravel;
    this.displayDialog = true;
  }

  returnBike(selectedStation: Station) {
    // ho stazione selezionata: oggetto
    console.log(selectedStation);

    if (selectedStation === undefined) {
      this.controlSelectStation = false;
      console.log(this.controlSelectStation);

    } else {

      this.serviceBike.takeTravelsId(this.selectedTravel).subscribe(res => {
        this.travelConcluded = <Travel>res;

        // modificare cambi di arrivo
        this.travelConcluded.dateArrival = Date.now();
        this.travelConcluded.idArrivalStation = selectedStation.idStation;
        this.travelConcluded.period = (this.travelConcluded.dateArrival - this.travelConcluded.dateDeparture);

        console.log(this.travelConcluded);

        this.serviceBike.insertTravel(this.travelConcluded).subscribe(() => {
          // console.log(res);
          console.log('prima di gotohome');
          console.log(selectedStation);
          this.goToHome();
        });
      });
    }
  }

  onDialogHide() {
    this.selectedTravel = null;
    this.controlSelectStation = null;
  }

  takeStations() {
    this.serviceBike.takeStation().subscribe(res => {
      console.log('take-stations');
      this.stations = <Station[]>res;
      console.log(this.stations);
    });
  }

  // il mio idUtente è l'username
  takeUser() {
    const token = localStorage.getItem('currentUser');
    let decodedToken: User;
    decodedToken = JWT(token);
    return decodedToken.username;
  }

  goToHome() {
    this.router.navigate(['home']);
  }

  // generazione codice per sblocco
  showCode() {
    this.msgs.push({severity: 'success', summary: this.translate.instant('Bike Returned'), detail: this.translate.instant('Lock Code')+`: ${this.randomInt()}`});
  }

  // generazione codice per sblocco
  randomInt(): number {
    return Math.floor(Math.random() * (900 - 100 + 1)) + 100;
  }


}
