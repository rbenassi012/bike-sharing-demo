import { Component, OnInit } from '@angular/core';
import {CardModule} from 'primeng/card';
import { FileUploadModule } from 'primeng/components/fileupload/fileupload';
import { User } from '../../model/models';
import { ServiceBikeService } from '../../service/service-bike.service';
import * as JWT from 'jwt-decode';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth/authentication.service';


@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  updateImg: string = "";
  user: User;
  updateProfile: boolean = false;

  constructor(private serviceBike: ServiceBikeService,
              private authenticationService: AuthenticationService,
              private router: Router) { }

  ngOnInit() {
    // func for decode token
    const username = this.getIdUser();

    this.serviceBike.takeUser(username).subscribe(res => {
      this.user = <User>res;
      console.log(this.user);
    });
  }

  getIdUser(): String {
    let decodedToken: User;
    let username: String;
    const token = localStorage.getItem('currentUser');
    console.log(token);

    decodedToken = JWT(token);
    username = decodedToken.username;
    return username;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['']);
  }

  update(){
    let tempImg = this.user.image;
    this.updateProfile = true;
    this.updateImg = `https://www.tinygraphs.com/squares/${tempImg}`;
  }

  generateImage(img : string){
    this.updateImg = `https://www.tinygraphs.com/squares/${img}`;
  }

  confirmUpdates(name: String, surname: String, phone: String, image: String){
    let user: User = new User();
    user.name = name;
    user.surname = surname;
    user.username = this.user.username;
    user.phone = phone;
    user.image = image;

    this.serviceBike.updateUser(user).subscribe(
      succ => {
        alert("Update completed");
        this.updateProfile = false;
        this.ngOnInit();
      },
      err => {
        console.log(err);
      }
    )
  }

}

/**
     * registration(data: any): any {
    let params = new HttpParams();
    // GOOD
    // For each param in object data append to request
    Object.keys(data).forEach(function (key) {
          params = params.append(key, data[key]);
    });
    // console.log(data);
    return this.http.post(environment.DB_USERS, <JSON>data);
  }
     */