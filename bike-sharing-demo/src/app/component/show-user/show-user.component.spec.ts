import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from "@angular/common";
import { ShowUserComponent } from './show-user.component';
import {CardModule} from 'primeng/card';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}
import {ButtonModule} from 'primeng/button';
import { RouterModule } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { RegisterComponent } from '../register/register.component';
import { ShowTravelsComponent } from '../show-travels/show-travels.component';
import {MessageModule} from 'primeng/message';
import {FieldsetModule} from 'primeng/fieldset';
import { StationComponent } from '../station/station.component';
import { MockComponent } from 'ng-mocks';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../routing/routing.module';
import {ChartModule} from 'primeng/chart';
import {GMapModule} from 'primeng/gmap';
import { TableModule } from 'primeng/table';
import {DialogModule, Dialog} from 'primeng/dialog';
import {MessagesModule} from 'primeng/messages';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import {InputMaskModule} from 'primeng/inputmask';



export function tokenGetter() {
  return localStorage.getItem('currentUser');
}


describe('ShowUserComponent', () => {
  let component: ShowUserComponent;
  let fixture: ComponentFixture<ShowUserComponent>;

  beforeEach(() => {
  TestBed.configureTestingModule({
    providers: [
      HttpClient,
      HttpHandler

    ],
    imports: [
      CardModule,
      RouterTestingModule.withRoutes(routes),
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
      ButtonModule,
      FieldsetModule,
      MessageModule,
      ChartModule,
      GMapModule,
      TableModule,
      DialogModule,
      MessagesModule,
      DropdownModule,
      FormsModule,
      InputMaskModule

    ],
    declarations: [
      ShowUserComponent,
      RegisterComponent,
      LoginComponent,
      HomeComponent,
      ShowTravelsComponent,
      StationComponent,
      ShowTravelsComponent
    ]
  })
  fixture = TestBed.createComponent(ShowUserComponent);
  component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
