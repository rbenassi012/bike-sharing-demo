import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Location } from "@angular/common";

//components
import { LoginComponent } from './login.component';
import { RegisterComponent } from '../register/register.component';
import { ShowTravelsComponent } from '../show-travels/show-travels.component';
import { HomeComponent } from '../home/home.component';

//services
import { HttpClient, HttpHandler } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { routes } from '../routing/routing.module';

//import primeng
import { FieldsetModule } from 'primeng/fieldset';
import { MessageModule } from 'primeng/message';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { MessagesModule } from 'primeng/messages';
import { FormsModule } from '@angular/forms';
import {ChartModule} from 'primeng/chart';
import {GMapModule} from 'primeng/gmap';
import {CardModule} from 'primeng/card';
import {InputMaskModule} from 'primeng/inputmask';

import {ButtonModule} from 'primeng/button';
import { RouterTestingModule } from '@angular/router/testing';
import { ShowUserComponent } from '../show-user/show-user.component';
import { StationComponent } from '../station/station.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { AuthenticationService } from 'src/app/service/auth/authentication.service';
import { User } from 'src/app/model/models';
import { Observable, of } from 'rxjs';


export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}

class MockAuthService extends AuthenticationService{

  login(username: String, password: String): Observable <boolean>{
    let ursControl = 'mb';
    let pwControl = '12';
    if(ursControl == username && pwControl == password){
      return of(true);
    }else{
      return of(false);
    }
  }
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: MockAuthService;

  let location = Location;


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler,
        MockAuthService
      ],
      imports: [
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        RouterTestingModule.withRoutes(routes),
        FieldsetModule,
        ButtonModule,
        MessageModule,
        InputMaskModule,
        TableModule,
        DialogModule,
        DropdownModule,
        MessagesModule,
        FormsModule,
        ChartModule,
        GMapModule,
        CardModule,
        BrowserAnimationsModule
      ],
      declarations: [
        LoginComponent,
        RegisterComponent,
        ShowTravelsComponent,
        HomeComponent,
        ShowUserComponent,
        StationComponent
      ]
    })
    //create component e test fixture
    fixture = TestBed.createComponent(LoginComponent);
    authService = TestBed.get(MockAuthService);
    //get test component from the fixture
    component = fixture.componentInstance;

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('errorCredential return false when password or username are incorrect', (done) => {

    let username: String = 'mb';
    let password: String = '12';

    authService.login(username, password).subscribe(
      x => {
        expect(x).toBe(true);
        done();
      },
      err => {

      })
  });

});
