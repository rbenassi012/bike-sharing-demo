import { Component, OnInit } from '@angular/core';
import { User } from '../../model/models';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth/authentication.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: String;
  password: String;
  user: User;
  image = 'https://www.mugstudio.it//images/portfolio/shike/logo.png';
  errorCredential = true;

  constructor(private router: Router,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  login(username: String, password: String) {
    this.authenticationService.login(username, password).subscribe(
      x => {
        this.user = x;
        this.errorCredential = true;
        this.router.navigate(['home']); },
      err => {
        this.errorCredential = false;
    });
  }

  registration() {
    this.router.navigate( ['register'] );
  }

  // control localstorage
  session(): boolean {
    if (localStorage.getItem('currentUser') === undefined) {
      return false;
    } else {
      return true;
    }
  }

}
