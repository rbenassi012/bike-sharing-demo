import { Component, OnInit, Input } from '@angular/core';
import { Bike } from '../../model/models';
import { ServiceBikeService } from '../../service/service-bike.service';

@Component({
  selector: 'app-show-bike',
  templateUrl: './show-bike.component.html',
  styleUrls: ['./show-bike.component.css']
})
export class ShowBikeComponent implements OnInit {

  details: Bike;

  constructor(private serviceBike: ServiceBikeService) { }

  // input array di bike da station-component
  @Input() elementBikes: String;

  ngOnInit() {
    this.getBike();
  }

  getBike() {
    console.log('entro in getBike');
    console.log(this.elementBikes);
    this.serviceBike.takeBike(this.elementBikes).subscribe(res => {
      this.details = <Bike>res;
      console.log(this.details);
      return <Bike>res;
    });
  }// getbike

}
