import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { ShowUserComponent } from '../show-user/show-user.component';
import { ShowTravelsComponent } from '../show-travels/show-travels.component';
import { AuthGardService as AuthGuard } from '../../service/auth/auth-gard.service';
import { HomeComponent } from '../home/home.component';

export const routes: Routes = [
  { path: '', component: LoginComponent},

  { path: 'register', component: RegisterComponent},

  { path: 'showTravels', component: ShowTravelsComponent,
    canActivate: [AuthGuard] },

  { path: 'home', component: HomeComponent,
    canActivate: [AuthGuard] },

  { path: 'showUser', component: ShowUserComponent,
    canActivate: [AuthGuard] },

  { path: '**', redirectTo: '', pathMatch: 'full', component: LoginComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
