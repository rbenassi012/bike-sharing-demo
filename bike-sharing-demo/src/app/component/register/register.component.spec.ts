import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';
//components
import { RegisterComponent } from './register.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AuthenticationService } from '../../service/auth/authentication.service';

//import prime ng
import {FieldsetModule} from 'primeng/fieldset';
import {MessageModule} from 'primeng/message';
import {ButtonModule} from 'primeng/button';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from '../routing/routing.module';
import { LoginComponent } from '../login/login.component';
import { HomeComponent } from '../home/home.component';
import { ShowTravelsComponent } from '../show-travels/show-travels.component';
import { ShowUserComponent } from '../show-user/show-user.component';
import { StationComponent } from '../station/station.component';
import { Chart } from '../../../../node_modules/chart.js/dist/Chart.js';
import {ChartModule} from 'primeng/chart';
import {GMapModule} from 'primeng/gmap';
import {DialogModule, Dialog} from 'primeng/dialog';
import {MessagesModule} from 'primeng/messages';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { FormsModule } from '@angular/forms';
import {CardModule} from 'primeng/card';
import { InputMaskModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler,
        AuthenticationService
      ],
      imports: [
        RouterTestingModule.withRoutes(routes),
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }),
      FieldsetModule,
      MessageModule,
      ButtonModule,
      ChartModule,
      GMapModule,
      DialogModule,
      MessagesModule,
      DropdownModule,
      TableModule,
      FormsModule,
      CardModule,
      InputMaskModule,
      BrowserAnimationsModule
      ],
      declarations: [
        RegisterComponent,
        LoginComponent,
        HomeComponent,
        ShowTravelsComponent,
        ShowUserComponent,
        StationComponent
      ]
      //schemas: [CUSTOM_ELEMENTS_SCHEMA]

    })
    //location = TestBed.get(Location)

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
