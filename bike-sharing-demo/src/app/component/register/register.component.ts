import { Component, OnInit } from '@angular/core';
import { User } from '../../model/models';
import { ServiceBikeService } from '../../service/service-bike.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth/authentication.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  controlUsername: boolean;
  controlPassword: boolean;


  imagePage = 'https://www.mugstudio.it//images/portfolio/shike/logo.png';

  constructor(private router: Router,
              private authenticationService: AuthenticationService) {}

  ngOnInit() {

  }

  // metodo di registrazione utente
  signUp(name: String, surname: String, username: String, email: String, password: String, phone: String, image: String) {

    // creazione ogg utente
    const user = new User();
    user.name = name.trim();
    user.surname = surname.trim();
    user.username = username.trim();
    user.email = email.trim();
    user.password = password.trim();
    user.phone = phone.trim();
    user.image = image.trim();

    if (/(?=.*[a-z])(?=.*[A-Z]).{8,15}/.test(user.password.toString()) ) {
      this.controlPassword = false;
      this.authenticationService.addUser(user).subscribe(
        () => {
          console.log('utente inserito');
          this.router.navigate(['login']);
          this.controlUsername = false;
        },
        () => {
          this.controlUsername = true;
          console.log('username già in uso');
        }
      );
    } else {
      this.controlPassword = true;
      console.log('devi inserire una password di almeno 8 caratteri');
    }
  }// metedo

}// classe
