import { Component, OnInit, OnDestroy } from '@angular/core';
import { Station, Bike, Travel, User } from '../../model/models';
import { ServiceBikeService } from '../../service/service-bike.service';
import {DialogModule, Dialog} from 'primeng/dialog';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { getLocaleDateFormat } from '@angular/common';
import { Message, MessageService } from 'primeng/api';
import * as JWT from 'jwt-decode';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.css'],
  providers: [MessageService]
})
export class StationComponent implements OnInit {

  stations: Array<Station>;
  details: Bike;
  travels: Travel;
  val: string;

  msgs: Message[] = [];
  borrowConfirm: boolean;
  displayDialog: boolean;
  selectedBike: String;
  selectedStation: String;

  constructor(private serviceBike: ServiceBikeService, private messageService: MessageService,
    private translate: TranslateService) { }

  ngOnInit() {
    this.getStations();
  }
  // chiamata servizio
  getStations() {
    console.log('get stations');
    this.serviceBike.takeStation().subscribe(res => {
      this.stations = res;
      });
  }

  onDialogHide() {
    this.selectedBike = null;
    this.msgs = [];
    console.log('onDialog');
    console.log(this.stations);
  }

  selectedBikeFunc(idBike: String, idStation: String) {
    console.log('selectedBikeFunc');

    this.selectedBike = idBike;
    this.getBikeS(idBike);
    this.selectedStation = idStation;
    this.displayDialog = true;
  }

  getBikeS(idBike: String) {
    console.log('getBikeS');
    this.borrowConfirm = null;
    this.serviceBike.takeBike(idBike).subscribe(res => {
      this.details = <Bike>res;
      console.log(this.details);
    });
  }

  borrowBikeId(idBi: String) {
    console.log('borrow');

    const travel = new Travel();
    travel.idBike = idBi;
    // sarà necessario leggere la data con una pipe per il formato corretto
    travel.dateDeparture = Date.now();
    travel.idDepartureStation = this.selectedStation;

    // decoded token jwt con libreria: npm i jwt-decoded
    // inserisco nel viaggio come idUtente l'username dell'utente
    const token = localStorage.getItem('currentUser');
    let decodedToken: User;
    decodedToken = JWT(token);
    travel.idUser = decodedToken.username;

    this.serviceBike.borrowBike(travel).subscribe(res => {
      console.log(res);
      this.borrowConfirm = true;
      this.msgs = [];

      this.msgs.push({severity: 'success', summary: this.translate.instant('Borrowed Bike'), detail: this.translate.instant('Unlock Code') + `: ${this.randomInt()}`});

      // this.msgs.push({severity: 'success', summary: "{{'Borrowed Bike' | translate }}", detail: `Unlock code: ${this.randomInt()}`});
    });
  }

  // generazione codice per sblocco
  randomInt(): number {
    return Math.floor(Math.random() * (900 - 100 + 1)) + 100;
 }

}
