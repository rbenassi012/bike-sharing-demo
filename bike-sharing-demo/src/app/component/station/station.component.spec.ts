import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationComponent } from './station.component';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FieldsetModule, CardModule, DialogModule, MessagesModule, InputMaskModule, ButtonModule } from 'primeng/primeng';

export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}

describe('StationComponent', () => {
  let component: StationComponent;
  let fixture: ComponentFixture<StationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler

      ],
      imports: [
        FormsModule,
        TranslateModule.forRoot({
          loader : {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        FieldsetModule,
        CardModule,
        DialogModule,
        MessagesModule,
        InputMaskModule,
        ButtonModule

      ],
      declarations: [
        StationComponent,

      ]
    })

    fixture = TestBed.createComponent(StationComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
