import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChartModule } from 'primeng/chart';
import { ServiceBikeService } from '../../service/service-bike.service';
import { Station } from '../../model/models';
import { TranslateService } from '@ngx-translate/core';

declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  data: any;
  stations: Array<Station>;
  stationsName = [];
  countBike = [];

  options: any;
  overlays: any[];
  // continene la stazione con il max di bici
  maxBike: Station;
  colors: Array<String>;

  constructor(private serviceBike: ServiceBikeService) { }

  ngOnInit() {
    this.takeStations();

    // creazione centro mappa
    this.options = {
      center: {lat: 41.8905198, lng: 12.4942486},
      zoom: 5
    };
  }

  takeStations() {
    this.serviceBike.takeStation().subscribe(res => {
      this.stations = <Station[]>res;

      this.serviceBike.takeColors().subscribe(
        res => {
          this.colors = <String[]>res;
          this.createData();
          this.createChart();
          this.createMap();
          // console.log(this.colors);
        },
        err =>{
          alert("Error");
          console.log(err)
        }
      );
    err =>{
      alert("Error");
          console.log(err)
    }
  });
  }

  createData() {

    let stationMaxBike = 0;

    for (let i = 0; i < this.stations.length; i++) {

      this.stationsName.push(this.stations[i].name);

      this.countBike.push(this.stations[i]['bikes'].length);

      // cerco stazione con max bici
      if (this.stations[i]['bikes'].length > stationMaxBike) {
        stationMaxBike = this.stations[i]['bikes'].length;
        this.maxBike = this.stations[i];
      }
    }
  }

  createChart() {
    this.data = {
      labels: this.stationsName,
      datasets: [
          {
            data: this.countBike,
            backgroundColor: this.colors
          }]
      };
  }// chart

  createMap() {
    // inserisco marker della stazione con più bici
    this.overlays = [
      new google.maps.Marker({position: {lat: this.maxBike.coordinates[0], lng: this.maxBike.coordinates[1]}, title: this.maxBike.name}),
    ];

  }
}
