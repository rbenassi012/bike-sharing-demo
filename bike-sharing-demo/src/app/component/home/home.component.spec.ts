import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

import {FieldsetModule} from 'primeng/fieldset';
import {ChartModule} from 'primeng/chart';
import {GMapModule} from 'primeng/gmap';
import { StationComponent } from '../station/station.component';
import {CardModule} from 'primeng/card';
import {DialogModule} from 'primeng/dialog';
import {InputMaskModule} from 'primeng/inputmask';
import {MessagesModule} from 'primeng/messages';

import { Chart } from 'Chart.js';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';


// node_modules/chart.js/dist/Chart.js

export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}

declare var google;


//Chart custom provider 
export function provideChart() {
  return new Chart();
 }

describe('HomeComponent', () => {
  let data;

  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  var google: any = {
    maps : {
        OverlayView : function () {
        },
        Marker : function () {
        },
        InfoWindow : function () {
        }
    }
  };

  beforeEach(() => {
    if(google){
      console.log("google exists");
    }
    TestBed.configureTestingModule({
      providers: [
        { provide: Chart, useFactory: provideChart }
      ],
      imports: [
        FieldsetModule,
        ChartModule,
        GMapModule,
        TranslateModule.forRoot({
          loader : {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        CardModule,
        DialogModule,
        InputMaskModule,
        FormsModule,
        MessagesModule,
        BrowserAnimationsModule,
        ButtonModule,
        HttpClientModule,
        
      ],
      declarations: [
        HomeComponent,
        StationComponent,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
