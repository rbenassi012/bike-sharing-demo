import { Component, OnInit, OnChanges } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthenticationService } from 'src/app/service/auth/authentication.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ProviderAst } from '@angular/compiler';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  items: MenuItem[];
  notShow: MenuItem[];

  constructor(private authenticationService: AuthenticationService,
              private router: Router,
              public translate: TranslateService) { }

  ngOnInit() {
    this.prova();
    this.translate.onLangChange.subscribe( res => this.prova());
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['']);
  }

  prova(){
    this.items = [];
    // chiamate per traduzione

    this.translate.get('Home').subscribe((res : string) => {
      this.items.push({label: res, icon: 'fas fa-map-marked-alt', routerLink: 'home'});
      console.log(res);
    });
    this.translate.get('Travels').subscribe((res: string) => {
      this.items.push({label: res, icon: 'fas fa-list-alt', routerLink: 'showTravels'});
    });
    this.translate.get('Profile').subscribe((res: string) => {
      this.items.push({label: res, icon: 'fa fa-fw fa-user', routerLink: 'showUser'});
      console.log(this.items);
    });
  }

  // control localstorage
  log(): boolean {
    if ( localStorage.getItem('currentUser') == null) {
      return false;
    } else {
      return true;
    }}
}// class

