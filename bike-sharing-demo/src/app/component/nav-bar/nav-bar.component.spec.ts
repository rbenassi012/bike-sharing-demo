import { TestBed } from '@angular/core/testing';
import { Location } from "@angular/common";
import { Router } from '@angular/router';

import { NavBarComponent } from './nav-bar.component';
import { LoginComponent } from '../login/login.component';
import { ShowTravelsComponent } from '../show-travels/show-travels.component';
import { ShowUserComponent } from '../show-user/show-user.component';

import { AuthGardService } from '../../service/auth/auth-gard.service';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpHandler, HttpClient} from '@angular/common/http'; //Provider

//PRime NG import
import { FormsModule } from '@angular/forms'; //To solve [(ngModel)] binding into input
import { ButtonModule, FieldsetModule, MessageModule } from 'primeng/primeng'; //To solve label binding
import { MenubarModule } from 'primeng/menubar';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { MessagesModule} from 'primeng/messages';
import { CardModule } from 'primeng/card';
import { RouterTestingModule } from "@angular/router/testing";


function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let location: Location;
  let router : Router;
  let fixture;

  let routes = [
    { path: '', component: LoginComponent },
    { path: 'showTravels', component: ShowTravelsComponent },
    { path: 'showUser', component: ShowUserComponent, canActivate: [AuthGardService]},
    { path: '**', redirectTo: '', pathMatch: 'full', component : LoginComponent}
  ]

  beforeEach(() => {
      TestBed.configureTestingModule({
        providers: [
            HttpClient,
            HttpHandler
        ],
        imports: [
            RouterTestingModule.withRoutes(routes),
            TranslateModule.forRoot({
                loader: {
                  provide: TranslateLoader,
                  useFactory: HttpLoaderFactory,
                  deps: [HttpClient]
                }
            }),
            FormsModule,
            ButtonModule,
            MenubarModule,
            FieldsetModule,
            MessageModule,
            TableModule,
            DialogModule,
            DropdownModule,
            MessagesModule,
            CardModule

        ],
        declarations: [
            NavBarComponent,
            LoginComponent,
            ShowTravelsComponent,
            ShowUserComponent
        ]
      })

      router = TestBed.get(Router);
      location = TestBed.get(Location);

      fixture = TestBed.createComponent(NavBarComponent);
      component = fixture.componentInstance;

      router.initialNavigation();
    })

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
