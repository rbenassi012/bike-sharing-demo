import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { NavBarComponent } from './component/nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import {MenubarModule} from 'primeng/menubar';
import {ButtonModule} from 'primeng/button';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { routes } from './component/routing/routing.module';
import { ShowTravelsComponent } from './component/show-travels/show-travels.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { RegisterComponent } from './component/register/register.component';
import { ShowUserComponent } from './component/show-user/show-user.component';
import { StationComponent } from './component/station/station.component';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule, MessagesModule, MessageModule, FieldsetModule, ChartModule, CardModule, InputMaskModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import {GMapModule} from 'primeng/gmap';

export function HttpLoaderFactory(http: HttpClient){
  return new TranslateHttpLoader(http, './assets/', '.json');
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        HttpClient,
        HttpHandler
      ],
      imports: [
        RouterModule,
        MenubarModule,
        ButtonModule,
        RouterTestingModule.withRoutes(routes),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        TableModule,
        DialogModule,
        DropdownModule,
        MessagesModule,
        MessageModule,
        FormsModule,
        FieldsetModule,
        GMapModule,
        ChartModule,
        CardModule,
        InputMaskModule
      ],
      declarations: [
        AppComponent,
        NavBarComponent,
        ShowTravelsComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        ShowUserComponent,
        StationComponent
      ],

      }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
