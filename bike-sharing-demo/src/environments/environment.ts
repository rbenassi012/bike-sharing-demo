// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  DB_USER: 'http://localhost:3000/users',
  DB_AUTH: 'http://localhost:3000/users/auth',
  DB_BIKE: 'http://localhost:3000/bikes',
  DB_TRAV: 'http://localhost:3000/travels',
  DB_TRAVID: 'http://localhost:3000/travel',
  DB_STAT: 'http://localhost:3000/stations',
  DB_COL: 'http://localhost:3000/colors'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
