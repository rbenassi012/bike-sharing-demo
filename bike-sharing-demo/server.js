import { readFileSync } from 'fs';
import { urlencoded, json } from 'body-parser';
import { create, router as _router, defaults } from 'json-server';
import { sign, verify } from 'jsonwebtoken';

const server = create()

const router = _router('./dbBike.json')

const db = JSON.parse(readFileSync('./dbBike.json', 'UTF-8'))

server.use(defaults());
//impostazioni
server.use(urlencoded({extended: true}))

server.use(json())
//firmare payload
const SECRET_KEY = '123456789'
//tempo di durata token
const expiresIn = '3h'

// Create a token from a payload
function createToken(payload){
  return sign(payload, SECRET_KEY, {expiresIn})
}

// Verify the token
function verifyToken(token){
  return  verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}

// Check if the user exists in database
function isAuthenticated({username, password}){
  return db.users.findIndex(user => user.username === username && user.password === password) !== -1
}

server.post('/users/auth', (req, res) => {
  const {username, password} = req.body;

  if (isAuthenticated({username, password}) === false) {
    const status = 401
    const message = 'Incorrect username or password!!!!!'
    res.status(status).json({status, message})
    console.log('errore SERVE');
    return
  }
  const access_token = createToken({username, password})
  res.status(200).json({access_token})
})

server.get('/users', (req, res) => {
  res.json(db.users);
})

server.get('/colors', (req, res) => {
  console.log('server colors');
  res.json(db.colors);
})

server.get('/stations', (req, res) => {
  res.json(db.stations);
})

server.get('/bikes', (req, res) => {
  res.json(db.bikes);
})

server.get('/travels', (req, res) => {
  res.json(db.travels);
})

server.get('/travels/:username', (req, res) => {
  var dbTravels = db.travels;
  var elementTravel;
  var response = [];
  for(let i=0; i<dbTravels.length; i++){
    elementTravel = dbTravels[i];
    if(elementTravel['idUser'] === req.params.username){
      response.push(elementTravel);
      //console.log(response);
      //console.log('---')
    }
  }
  res.json(response);
})

//ho cambiato l'url perchè mi chiamava sempre la stessa funzione
server.get('/travel/:idTravel', (req, res) => {
  var dbTravels = db.travels;
  var elementTravel;
  var response = [];
  console.log('entro nel travel/:id');

  for(let i=0; i<dbTravels.length; i++){

    elementTravel = dbTravels[i];
    console.log(elementTravel)
    if(elementTravel['idTravel'] === req.params.idTravel){
      response = elementTravel;
      //console.log(response);
      console.log('---')
    }
  }
  res.json(response);
})

server.get('/bikes/:id', (req, res) => {
  var dbBike = db.bikes;
  var response;
  var elementBike;

  for(let i=0; i<dbBike.length; i++){
    elementBike = dbBike[i];
    //console.log(i);
    //console.log(dbBike[i]);

    if(elementBike['numTelaio'] === req.params.id){
      //console.log('req.params.id'+req.params.id);
      response = elementBike;
    }
  }
  res.json(response);
})

server.get('/users/:username', (req, res) => {

  var dbUsers = db.users;
  var response;

  for(let i=0; i<dbUsers.length; i++){
    if(dbUsers[i].username === req.params.username){
      response = dbUsers[i];
      //console.log(response)
    }
  }
  res.json(response);
})

server.post('/users/:username', (req, res) => {
  var dbUsers = db.users;
  var update = null;

  for(let i=0; i<dbUsers.length; i++){
    if(dbUsers[i].username === req.params.username){
      update = dbUsers[i];
      //console.log(response)
    }
  }

  if(update != null){
    update.name = req.body.name;
    update.surname = req.body.surname;
    update.phone = req.body.phone;
    update.image = req.body.image;
    res.status(200).json({'status' : 'OK aggiornamento utente'})
  }
})

server.post('/users', (req, res) => {
  console.log(db.users);

  var idFakeUser = db.users.length;
  req.body.id = idFakeUser;
  let control = false;

  var userControlUsername = req.body.username;

  //console.log(userControlUsername);

  for(let i=0;i<db.users.length;i++){
    if(userControlUsername === db.users[i]['username'] ){
      console.log('trovato--> non è possibile inserire un utente con questo username');
      control = true;
    }
  }
  //controllo su username
  if(control === true){
    res.status(409).json({'status' : 'Già presente'})
  }else{
    db.users.push(req.body);
    res.status(200).json({'status' : 'OK inserimento utente'})
  }
  //console.log(control);
})

server.post('/travel', (req, res) => {

  console.log(req.body);

  //DEPOSITO BICI
  var dbTravel = db.travels;
  //salvo dell'oggetto richiesta idTravel, idStation, idBike
  //cioè i campi che devo modificare rispettivamente in db.travels,
  //db.Stations, db.Bike
  var idTravel = req.body.idTravel;
  var idStation = req.body.idArrivalStation;
  //console.log(idStation);
  //mi salvo id Bike del Viaggio
  var idBike = req.body.idBike;
//salvo bici da spostare
  var bikeMove;

  //inserisco il viaggio con campi completi (id/data Arrivo, durata)con id corrispondente
  for(let i =0; i<dbTravel.length; i++){
    if(dbTravel[i]['idTravel'] === idTravel){
      dbTravel[i] = req.body;
    }
  }

  //set campo free di Bikes a true
  for(let i=0; i<db.bikes.length; i++){
    //tutt
    if(db.bikes[i]['numTelaio'] === idBike){
      db.bikes[i]['free'] = true;
    }
  }
  console.log('deposito bici');
  //sposto bici nelle stazione stazione di arrivo e la elimino in quella
  //di partenza
  //Elimino bici stazione partenza
  for(let i=0; i< db.stations.length; i++){
    var dbStation = db.stations;
    var arrayBike = dbStation[i]['bikes'];

    for(let j=0; j<arrayBike.length; j++){
      if(arrayBike[j].idBike === idBike){
        bikeMove = arrayBike[j];
        arrayBike.splice(j, 1);

        console.log('splice')

      }
    }
  }
  //inserisco bici stazione arrivo
  for(let i=0; i<db.stations.length; i++){
    var dbStation = db.stations;
    var arrayBike = dbStation[i]['bikes'];
    console.log(dbStation[i]['idStation'])
    console.log(idStation)

    console.log('inserisco bici stazione')
    if(dbStation[i]['idStation'] === idStation){
      console.log(bikeMove)
      dbStation[i]['bikes'].push(bikeMove);
      console.log('push bici nell stazione arrivo')
    }
  }

  res.status(200).json({'status' : 'OK inserimento'})

})


server.post('/travels', (req, res) => {
  //prendo in prestito
  console.log('post travels');
  //creo manualmento idTravel perchè non lo autogenera
  var idFakeTravel = db.travels.length;
  req.body.idTravel = idFakeTravel + '';

  //mi salvo id Bike del Viaggio
  var idBike = req.body.idBike;

  //inserimento nel json del viaggio
  db.travels.push(req.body);

  //set campo free di Bikes a false PER FINTA
  for(let i=0; i<db.bikes.length; i++){
    //tutt
    if(db.bikes[i]['numTelaio'] === idBike){
      db.bikes[i]['free'] = false;
    }
  }
  res.status(200).json({'status' : 'OK inserimento'})

})

server.use(/^(?!\/auth).*$/,  (req, res, next) => {
  if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
    const status = 401
    const message = 'Bad authorization header!!!'
    res.status(status).json({status, message})
    return
  }
  try {
     verifyToken(req.headers.authorization.split(' ')[1])
     next()
  } catch (err) {
    const status = 401
    const message = 'Error: access_token is not valid'
    res.status(status).json({status, message})
  }
})

server.use(router)

server.listen(3000, () => {
  console.log('Run Auth API Server')
})

server.use('/api', router);
